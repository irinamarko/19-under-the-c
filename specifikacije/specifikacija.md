Slucaj upotrebe Prvi nivo
Kratak opis: Igrac zapocinje prvi nivo klikom na dugme “Level 1” iz glavnog menija ili klikom dugme "start". Aplikacija inicializuje nivo i igra krece. Nivo se zavrsava: ako nakon isteka vremena igrac ima vise od 70 poena, gubitkom svih zivota ili koriscnjem precica igrice.
Akteri: Igrac – igra nivo pomeranjem objekta igraca u datom okruzenju
Preduslovi: Aplikacija je pokrenuta 
Postuslovi: U slucaju uspesnog prelaska nivoa broj preostalih zivota se prenosi u naredni nivo.
Osnovni tok:
1.Igrac klikce na dugme “Level 1” ili "Start"iz glavnog menija 
2.Aplikacija inicializuje nivo iscrtavajuci interaktivne objekte, neprijatelje, igraca. 
3.Aplikacija postvlja broj zivota.
4.Aplikacija inicijalizuje tajmer(vreme)
5.Nakon inicializacije igra krece i korisnik moze da upravlja objektom koji predtavlja igraca
6.Sve dok vreme ne istekne :
	6.1. Aplikacija proverava da li je doslo do sudara igraca i neprijatelj
		6.1.1. Ukoliko jeste oduzima se jedan health. Prelazi na korak 6.2
	6.2 Aplikacija proverava da li je doslo do sudara igaca i kljuca
		6.2.1. Ukoliko jeste dodaje se 2 dva poena.Prelazi se na korak 6.3
	6.3.Aplikacija proverava da li doslo do kontakta vatre i vrane
		6.3.1. Ukoliko jeste dodaje se jedan poen.Prelazi se na korak 6.4
	6.4. Provera se da li doslo kontakta igraca i specijalnog napada vrane
		6.4.1. oduzima se jedan healt.Prelazi se na korak 6.5
	6.5.Provera se da li broj izgubljenih healt jednak 3
		6.5.1 Ukoliko jeste oduzima se jedan zivot
		6.5.2. Broj izgubljenih health postaje 0. Prelazi se na  korak 6.6
	6.6.Aplikacija proverava da li je broj zivota jednak 0
		6.6.1. Ukoliko jeste igrica se prekida 
		6.6.2 Otvara se startni meni, povratak na korak 1
	7.Pri isteku vremena aplikacija provera broj poena
		7.1 Ukoliko je broj poena veci ili jednak 70 
			7.1.1 Prelazi se na drugi nivo
		7.2 Ukoliko je broj poena manji od 70
			7.2.1 Aplikacija proverava da je broj zivota veci od 0
				7.1.1.1 Ukoliko jeste , niko krece iznova. Prelazi se na korak1
			7.2.2 Ukoliko je broj poena manji od 0
			              7.2.2.1 Igra se prekoda i ide se na startni meni. Prelazi se na 2
 
Alternativni tokovi: U slucaju pritiska dugmeta esc ili klikom na dugme X aplikacija zavrsava s radom i sav ostvaren napredak se zaboravlja. U  slucaju pritiska neke od precica 1, 3 ili 4 zavrsava se ovaj slucaj upotrebe i prelazi na precicom odredjen slucaj upotrebe(Slucaj upotrebe “Prvi nivo”, “Treci nivo” ili “Cetvrti nivo”) gde je broj zivota inicijalizovan na 3. U slucaju pritiska tastera 2 pokrece se Drugi nivo s brojem zivota inicijalizovanim na 3.
Dodatne informacije: Tokom izvrsavanja ovog nivoa bez koriscenja precia, aplikacija ce pamtiti broj zivota tokom svakog nivoa.

----------------------------------------------------------

Slucaj upotrebe Drugi nivo
Kratak opis: Igrac zapocinje drugi nivo klikom na dugme “Level 2” iz glavnog menija ili prelaskom prvog
nivoa. Aplikacija inicializuje nivo i igra krece. Nivo se zavrsava: pobedom glavnog neprijatelja, gubitkom
svih zivota ili koriscnjem precica igrice.
Akteri: Igrac – igra nivo pomeranjem objekta igraca u datom okruzenju
Preduslovi: Aplikacija je pokrenuta
Postuslovi: U slucaju uspesnog prelaska nivoa broj preostalih zivota se prenosi u naredni nivo.
Osnovni tok:
1. Igrac klikce na dugme “Level 2” iz glavnog menija ili predje prvi nivo
2. Aplikacija inicializuje nivo iscrtavajuci platforme, interaktivne objekte, neprijatelje, igraca.
3. Aplikacija postvlja broj health-a na 5.
4. Nakon inicializacije igra krece i korisnik moze da upravlja objektom koji predtavlja igraca
5. Sve dok nivo nije predjen naredni koraci se ponavljaju:
5.1. Igrac se pomera po sceni nivoa u skladu s komandama koje korisnik salje putem tastature.
5.1.1. Ukoliko je pomeraj igraca van logike nivoa(npr. igrac pokusava da prodje kroz levu
stranu platforme) ili je pod novonastalim uslovima potrebno izvrsiti dodatno
pomeranje igraca(npr. padanje igraca s platforme) aplikacija vrsi korigovanje tako da
logika ostane ispostovana.

5.2. Aplikacija proverava da li je igrac izvrsio napad i ako jeste iscrtava mac igraca.
5.3. Aplikacija proverava da li je igrac dosao u aktivacionu sredinu nekog od neprijatelja duhova:
5.3.1. Ukoliko igrac jeste u neposrednoj sredini bilo kog neprijatelja duha, tada neprijatelj
duh prelazi u svoje aktivaciono stanje i prati igraca po sceni
5.4. Aplikacija proverava da li se igrac nalazi u aktivacionom polju “laser pauka”:
5.4.1. Ukoliko se igrac nalazi u sredini gde je laser od neprijatelja pauka aktivan tada pauk
ispaljuje svoje lasere ka igracu.
5.4.2. Aplikacija proverava da li je igrac izasao iz sredine lasera i ako jeste “laser pauk”
prestaje s napadima.

5.5. Aplikacija proverava da li je neki od neprijatelja pogodjen macem igraca:
5.5.1. Ukoliko neki od neprijatelja jeste pogodjen macem igraca onda se health neprijatelja
smanjuje.
5.5.1.1. Proverava se da li pogodjeni neprijatelj ima jos healtha i ukoliko nema brise se
sa scene nivoa.

5.6. Aplikacija proverava da li je igrac dosao u koliziju s nekim od neprijatelja ili napadima
neprijatelja:
5.6.1. Ukoliko igrac jeste dosao u koliziju s nekim od neprijatelja ili napadima neprijatelja
onda se igracev health smanjuje.
5.6.1.1. Proverava se da li igrac ima jos health-a nakon smanjivanja
5.6.1.1.1. Ukoliko igrac nema vise health-a onda se proverava da li igrac ima jos

zivota
5.6.1.1.1.1. Ukoliko igrac nema vise zivota aplikacija se vraca u glavni meni.
5.6.1.1.1.2. Ukoliko igrac ima jos zivota ide se na korak 2

5.7. Aplikacija proverava da li je igrac dosao u koliziu s trnjem:
5.7.1. Ukoliko igrac jeste dosao u koliziju s trnjem ponovo se iscrtava na specijalnim
mestima i smanjuje mu se health.
5.7.1.1. Ide se na korak 5.6.1.1.
5.8. Aplikacija proverava da li je igrac dosao u koliziju s zutim kristalom:
5.8.1. Ukoliko igrac jeste dosao u koliziju s zutim kristaloom, Kristal se brise s scene i
povecava se broj u levom gornjem uglu koji prebrojava zute kristale.
5.9. Aplikacija proverava da li je igrac dosao u koliziju s ljubicastim kristalom:
5.9.1. Ukoliko igrac jeste dosao u koliziju s ljubicastim kristalom taj se brise sa scene i broj
skupljenih ljubicastih kristala se povecava.

5.10. Aplikacija proverava da li je igrac dosao u koliziju s napitkom:
5.10.1. Ukoliko igrac jeste dosao u koliziju s napitkom , napitak se brises a scene a broj
zivota se povecava.

5.11. Aplikacija proverava da li je igrac dosao u koliziju s ljubicastim vratima:
5.11.1. Ukoliko igrac jeste dosao u koliziju s ljubicastim vratima i sakupio je sve ljubicaste
kristale iz nivoa onda se ljubicasta vrata brisu sa scene tako da igrac moze tuda da
prodje

5.12. Aplikacija proverava da li je igrac dosao u koliziju s zutim vratima:
5.12.1. Ukoliko igrac jeste dosao u koliziju s zutim vratima a pritom je sakupio sve zute
kristale onda se zuta vrata brisu sa scene i igrac moze tuda da prodje.
5.13. Aplikacija proverava da li je igrac dosao u sredinu glavnog neprijatelja
5.13.1. Ukoliko igrac jeste dosao u sredinu glavnog neprijatelja krece glavna borba
5.13.2. Ukoliko glavni neprijatelj vise nema health-a nivo je predjen i prelazi se na slucaj
upotrebe “Treci nivo” s brojem preostalih zivota iz drugog nivoa a health-om koji
inicializuje treci nivo

Alternativni tokovi: U slucaju pritiska dugmeta esc ili klikom na dugme X aplikacija zavrsava s radom i
sav ostvaren napredak se zaboravlja. U slucaju pritiska neke od precica 1, 3 ili 4 zavrsava se ovaj slucaj
upotrebe i prelazi na precicom odredjen slucaj upotrebe(Slucaj upotrebe “Prvi nivo”, “Treci nivo” ili
“Cetvrti nivo”) gde je broj zivota inicijalizovan na 3. U slucaju pritiska tastera 2 pokrece se Drugi nivo s
brojem zivota inicijalizovanim na 3.
Dodatne informacije: Tokom izvrsavanja ovog nivoa bez koriscenja precia, aplikacija ce pamtiti broj
zivota tokom svakog nivoa.


----------------------------------------------------------
Slucaj upotrebe za 3. nivo

kratak opis : 
Igrac zapocinje igru tako sto iz glavnog menija aplikacije izabere konkretno ovaj nivo ili predje 1. i 2. da bi se doslo  do ovog. Pamte se zivoti od prethodnih nivoa ako je to slucaj i zapocinje se nivo. Nakon odigranog nivoa ili se nastavlja na sledeci nivo, ili se vraca na glavni meni u slucaju gubitka.

akteri :
Igrac - igra nivo

preduslovi: 
Aplikacija je pokrenuta i dodjeno je preko 1. i 2. do 3. nivoa ili je pokrenit on sam.

Postuslovi: U slucaju gubitka zivota vraca se na glavni meni, u suprotnom se cuva stanje zivota za
naredni nivo.


Osnovni tok:

1.Igrac iz glavnog menija bira dugme “level 3” i ima 3 zivota i 3 health poena
 1.1. Pokrece se “ level 3”
 1.2. Igrac se pomera levo, desno,gore,dole pomocu strelica na tastaturi
 1.3. Igrac  da puca na dugme “Space”
 1.4. klikom na dugme “S” 
   1.4.1. Stvara sebi stit( zastitu od neprijatelja) i oduzima se 10 poena 
   1.4.2. Ne desava se nista jer igrac nema >= 10 poena
 1.5.  Igrac je dosao u kontakt sa neprijsteljem i gubi 1 “health “ poen
     1.5.1. Igrac je na 0 health poena
      1.5.1.1. Igrac je na 0 zivota, igra se zavrsava i vraca na glavni meni, prelazi na korak 1
      1.5.1.2. Igracu se oduzima 1 zivot i ponovo zapocinje nivo - prelazak na korak 1.1
  1.6. Igrac pobedjuje neprijatelja i prelazak na nivo 4.

2.Igrac iz glavnog menija bira dugme “Level 1” ili “Level 2”
 2.1. Igrac dobija 3 zivota i prenosi se stanje u prethodne nivoe
   2.1.1. Izgubljena sva 3 zivota tokom igranja-prelazak na korak 1.
   2.1.2. Predjenj nivoi 1 i 2, prenos broja zivota u level 3 - prelazak na korak 1.1.
3.Igrac igra “Level 4”
 3.1. Igrac pobedjuje, povratak na glavni meni, prelazak na korak 1
 3.2. Igrac je izgubio, povratak na glavni meni, prelazak na korak 1
 
4.Igrac bira dugme “Exit” i gasi se aplikacija

Alternativni tok:
A1: Neocekivani izlaz kz aplikacije. Ukoliko korisnik u bilo kom trenutku izadje iz aplikacijs, sav eventualan zapamcen proces se
brise, i aplikacija zavrsava sa radom.
Slucaj upotrebe se zavrsava.

Dodatne informacije: Tokom izvrsavanja ovog nivoa bez koriscenja precia, aplikacija ce pamtiti broj zivota tokom svakog nivoa.





🍓 Demo snimak: https://drive.google.com/file/d/1qKwlbRyV70uofJHHg3TX2vzwWxK67p-0/view?usp=drive_web 
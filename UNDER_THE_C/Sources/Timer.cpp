#include "Headers/Timer.h"
#include <QFont>

Timer::Timer(QGraphicsItem *parent):QGraphicsTextItem(parent)
{
    number_of_second=60;
    setPlainText(QString("  ") + QString::number(number_of_second));
    setDefaultTextColor(Qt::black);
    setFont(QFont("times", 16));

}

void Timer::nextSecond()
{   if(number_of_second>0){
    number_of_second--;
    if(number_of_second>10){
    setPlainText(QString("   ") + QString::number(number_of_second));
    }else{
        setPlainText(QString("   ") + QString::number(number_of_second));
        setDefaultTextColor(Qt::red);
    }

    }
}


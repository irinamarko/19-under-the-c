#include "Headers/Sword.h"

Sword::Sword()
{
    setPixmap(QPixmap(":/images/attacks/sword/sword1.png"));
    setScale(0.1);
    existance = 0;
    isAlive = false;
}

void Sword::move(int player_x, int player_y, int left, int right, int side)
{
    if(left == 1){
        setPos(player_x -75, player_y+5);
    }
    else if(right == 1){
        setPos(player_x + 15, player_y + 5);
    }
    else if(side == 2){
        setPos(player_x, player_y+5);
    }
    else
        setPos(player_x - 60, player_y +5);

    if(existance > 0)
        image_change(left, right, side);
}
void Sword::image_change(int left, int right, int side)
{
    if(existance == 5){
        existance = -1;
    }
    else {
        if(left == 1 || (right != 1 && side != 2)){
            switch (existance) {
            case 1:
                setPixmap(QPixmap(":/images/attacks/sword/sword_left_1.png"));
                break;
            case 2:
                setPixmap(QPixmap(":/images/attacks/sword/sword_left_2.png"));
                break;
            case 3:
                setPixmap(QPixmap(":/images/attacks/sword/sword_left_3.png"));
                break;
            case 4:
                setPixmap(QPixmap(":/images/attacks/sword/sword_left_4.png"));
                break;
            }
        }
        else{
            switch (existance) {
            case 1:
                setPixmap(QPixmap(":/images/attacks/sword/sword1.png"));
                break;
            case 2:
                setPixmap(QPixmap(":/images/attacks/sword/sword2.png"));
                break;
            case 3:
                setPixmap(QPixmap(":/images/attacks/sword/sword3.png"));
                break;
            case 4:
                setPixmap(QPixmap(":/images/attacks/sword/sword4.png"));
                break;
            }
        }
        existance++;
    }
}

int Sword::getExistance()
{
    return existance;
}

void Sword::setExistance(int number)
{
    existance=number;
}

bool Sword::getIsAlive()
{
    return isAlive;
}

void Sword::setIsAlive(bool alive)
{
    isAlive = alive;
}

#include "Headers/Bubble.h"
#include <QTimer>
#include <Headers/Game.h>
#include <QGraphicsScene>
#include <Headers/Enemy.h>
//int Bubble::counter=0;
Bubble::Bubble(QGraphicsItem *parent): StraightMotion(parent){

    setIsAlive(true);
    setType(BUBBLE);

    setPixmap(QPixmap(":/images/attacks/bubble.png"));
    setScale(0.06);
    dark=false;
    health_change=0;

}

void Bubble::collesion()
{
    auto colliding_items= collidingItems();
        foreach(auto item, colliding_items){
            if(typeid (*item)== typeid(PlayerMermaid) ){

             if(qgraphicsitem_cast<PlayerMermaid *>(item)->getShiled()==false){
                setHealthChange(1);
             }
             else{
                qgraphicsitem_cast<PlayerMermaid *>(item)->setShiled(false);
                setHealthChange(0);
             }

            setIsAlive(false);
            }
       }
}


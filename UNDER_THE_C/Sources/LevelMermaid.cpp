#include "Headers/LevelMermaid.h"
#include "Headers/Bubble.h"
#include "Headers/PlayerMermaid.h"
#include "QSet"
#include "QRandomGenerator"
#include <iostream>


LevelMermaid::LevelMermaid(QObject *parent):
    LevelScene(parent)
{}


void LevelMermaid::initialize()
{

    musicButton= new Button();
    musicButton->initialize("://images/buttons/speaker.png", 0.03, 5, 55);
    addItem(musicButton);

    if(mute==true)
        musicButton->initialize("://images/buttons/mute.png", 0.03, 5, 55);

    buttonRules=new Button();
    buttonRules->initialize(":/images/buttons/PinkyButton.png", 1, 500,500);
    connect(buttonRules, SIGNAL(clicked()),this, SLOT(onClicked()) );

    Button::connect(musicButton, SIGNAL(clicked()),this, SLOT(onSetMute()));
    setSceneRect(0,0,1280,720);
    setBackgroundBrush(QPixmap(":/images/backgrounds/sea2.jpg").scaled(1280,720));
    player= new PlayerMermaid();
    addItem(player);
    setFocus(Qt::TabFocusReason);
    player->setFocus();
    player->setPos(0,height()/2);
    timeCounter=0;
    //ovo ce nasldjivati kada se budu spojili nivoi
    pScore = new Score();
    addItem(pScore);
    //ovo ce nasldjivati kada se budu spojili nivoi
    pHealth = new Health();
    pHealth->setPos(pHealth->x(), pHealth->y()+25);
    addItem(pHealth);
    //sounds();
    speed_items=0;
    moveLeft=false ;
    moveRight=false;
    moveUp=false ;
    moveDown=false;
    addedOctopus=false;
    pause=false;
    stopEventKey=false;
    onRules=false;
    pushBackSound("qrc:/sounds/mermaid.wav");
    pushBackSound("qrc:/sounds/fire.mp3");
    checkMute();
    finalEnemy=false;
    end=false;
    positionPlayer=false;





}


void LevelMermaid::onSetMute()
{

     Music::setMute();
    if(mute==true)
        musicButton->initialize("://images/buttons/mute.png", 0.03, 5, 55);
    else
        musicButton->initialize("://images/buttons/speaker.png", 0.03, 5, 55);
    checkMute();
}

void LevelMermaid::onClicked()
{
    removeItem(rules);
    removeItem(buttonRules);
    pause=!pause;
    emit gamePaused();
    playSounds();
    stopEventKey=false;
    onRules=false;
}

void LevelMermaid::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_P && !onRules){
       emit gamePaused();
        if(!pause ){
            stopEventKey=true;
            pauseSonuds();
        }else{
            stopEventKey=false;
            playSounds();

        }
        pause=!pause;

    }else if(event->key()==Qt::Key_M){

       setMute();
       if(mute==true)
           musicButton->initialize("://images/buttons/mute.png", 0.03, 5, 55);
       else
           musicButton->initialize("://images/buttons/speaker.png", 0.03, 5, 55);
       checkMute();
    }
    if(event->key()==Qt::Key_B){
        clean_mem();
        emit gameOver();
    }
    if(event->key()==Qt::Key_1){
        clean_mem();
        emit level(0);
        return;
    }
    if(event->key()==Qt::Key_2){
        clean_mem();
        emit level(1);
    }
    if(event->key()==Qt::Key_3){
        clean_mem();
        emit level(2);
    }
    if(event->key()==Qt::Key_4){
        clean_mem();
        emit level(3);
    }

    if(event->isAutoRepeat() == false && event->key()==Qt::Key_H && !onRules){

        if(!pause){

            emit gamePaused();
            stopEventKey=true;
            pauseSonuds();
            pause=!pause;
        }
        rules->setNumberOfLevel(3);
        buttonRules->setZValue(10);
        addItem(rules);
        addItem(buttonRules);
        onRules=true;


    }
    if(stopEventKey)
        return;

    if (event->key()==Qt::Key_Left){
           moveLeft=true;
       }

       else if (event->key()==Qt::Key_Right){
         moveRight=true;
    }
       else if (event->key()==Qt::Key_Up){
          moveUp=true;
       }
       else if (event->key()==Qt::Key_Down){
         moveDown=true;
       }

    else if(event->key()==Qt::Key_Space && event->isAutoRepeat()==false){
        Bubble *bubble = new Bubble();
        items.insert(bubble->getID(),bubble);
        bubble->setPos(player->x()+130, player->y()+60);
        addItem(bubble);
        makeSound(1);
        bubble->incID();
    }
    else if(event->key()==Qt::Key_S){
       if(pScore->getScore() >= 10){
       pScore->decreaseByPoints(10);
       player->setShiled(true);


       }
    }


}

void LevelMermaid::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left){
        moveLeft=false;
    }

    if(event->key() == Qt::Key_Right){
        moveRight=false;
    }
    if(event->key() == Qt::Key_Up){
        moveUp=false;
    }

    if(event->key() == Qt::Key_Down){
        moveDown=false;
    }


}


void LevelMermaid::update()
{

   player->image_change();
   if(pScore->getScore()>70)
        finalEnemy=true;

//ENEMY
  if(!end){
    if(finalEnemy){
        if(!addedOctopus){
            enemyOctopus= new EnemyOctopus();
            addItem(enemyOctopus);
            addItem(enemyOctopus->healthBar->barFrame);
            addItem(enemyOctopus->healthBar->bar);
            addedOctopus=true;
            items.insert(enemyOctopus->getID(),enemyOctopus);
            enemyOctopus->incID();
        }


    }

    if(timeCounter%22==0 ){
           EnemyofMermaid *enemy = new EnemyofMermaid();
           addItem(enemy);
           addItem(enemy->healthBar->barFrame);
           addItem(enemy->healthBar->bar);
           items.insert(enemy->getID(),enemy);
           enemy->incID();

       }
    if(timeCounter%35==0 && !finalEnemy){
           Shell *shell= new Shell();
           addItem(shell);
           items.insert(shell->getID(), shell);
           shell->incID();
    }

     if(addedOctopus && timeCounter%20==0){

           Ink *ink = new Ink();
           items.insert(ink->getID(), ink);
           ink->setPos(enemyOctopus->x()+10, enemyOctopus->y()+100);
           addItem(ink);
           ink->incID();
    }
    if(addedOctopus && timeCounter%30==0){

           Ink *ink = new Ink();
           items.insert(ink->getID(), ink);
           ink->setPos(enemyOctopus->x()+120, enemyOctopus->y()+40);
           addItem(ink);
           ink->incID();
    }
    if(addedOctopus && timeCounter%40==0){

           Ink *ink = new Ink();
           items.insert(ink->getID(), ink);
           ink->setPos(enemyOctopus->x()+600, enemyOctopus->y()+200);
           addItem(ink);
           ink->incID();
    }
    if(addedOctopus && timeCounter%50==0){

           Ink *ink = new Ink();
           items.insert(ink->getID(), ink);
           ink->setPos(enemyOctopus->x()+590, enemyOctopus->y()+400);
           addItem(ink);
           ink->incID();
    }



    // Deo za pomeranje objekata, obradu njihovih kolizija i po potrebi menjanje health i score igraca
    if(!items.isEmpty()){
            for(auto it=items.begin(); it!=items.end(); it++){

                if(addedOctopus && it.value()->getType() == ItemOfScene::typeOfItem::ENEMYOCTOPUS ){
                    if(enemyOctopus->getTimeout()==0){

                    enemyOctopus->move();
                    }
                    else{
                        enemyOctopus->decraseTimeout();

                    }
                    enemyOctopus->image_change();
                    if(enemyOctopus->getScoreChange()>0){
                        pScore->increaseByPoints(enemyOctopus->getScoreChange());
                        enemyOctopus->setScoreChange(0);
                    }
                    if(enemyOctopus->getHealthChange() > 0){
                        enemyOctopus->setHealthChange(0);
                        pHealth->decrease();
                        if(pHealth->getHealth() <= 0){
                           clean_mem();
                           emit lifeLost();
                           return;
                        }
                    }

                    if(enemyOctopus->getIsAlive()==false){
                        end=true;
                        clean_mem();
                        return;
                    }
                }

                else if(it.value()->getType() == ItemOfScene::typeOfItem::SHELL){
                    auto shell = (qgraphicsitem_cast<Shell *>(it.value()));

                    if(shell->getOpened())
                        shell->move(0, 2, height());
                    else
                        shell->move(1, 6, 0);
                    shell->collision();

                    // Provera da li treba povecati score(da li je igrac ubio neprijatelja)
                    if(shell->getScoreChange() > 0){
                        pScore->increaseByPoints(10);
                        shell->setScoreChange(0);
                    }
                }


                else if(it.value()->getType() == ItemOfScene::typeOfItem::INK){
                    auto ink = (qgraphicsitem_cast<Ink *>(it.value()));


                        ink->move(1, 10, 0);
                        ink->collision();
                        if(ink->getHealthChange() > 0){
                            pHealth->decrease();
                            if(pHealth->getHealth() <= 0){
                               clean_mem();
                               emit lifeLost();
                               return;
                            }
                        }
                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::BUBBLE){


                    auto bubble = (qgraphicsitem_cast<Bubble*>(it.value()));
                    if(bubble->dark==false)
                    bubble->move(2, 5, width());
                    else{
                        bubble->move(1,20,0);
                        bubble->collesion();
                        if(bubble->getHealthChange() > 0){
                            pHealth->decrease();
                            if(pHealth->getHealth() <= 0){
                               clean_mem();
                               emit lifeLost();
                               return;
                            }
                    }
                }
               }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::ENEMYOFMERMAID){

                    auto enemy = (qgraphicsitem_cast<EnemyofMermaid*>(it.value()));

                    enemy->move();

                    if(enemy->getScoreChange() > 0){
                        pScore->increaseByPoints(1);
                        enemy->setScoreChange(0);
                    }

                    if(enemy->getHealthChange() > 0){
                        pHealth->decrease();
                        if(pHealth->getHealth() <= 0){
                           clean_mem();
                           emit lifeLost();
                           return;
                        }
                    }
                }

            }
    }

    /*** BRISANJE ELEMENATA ******/
    int key_check = -1;

    if(!items.isEmpty()){
        for(auto it=items.begin(); it!=items.end(); it++){

            if(key_check != -1){
                auto value = items.take(key_check);
                removeItem(value);
                delete value;

                key_check = -1;
            }

            if(it.value()->getIsAlive() == false){
                key_check = it.key();
            }
        }
    }

    if(key_check != -1){
        auto values=items.take(key_check);
        removeItem(values);
        delete values;

        key_check = -1;
    }


    if(moveLeft){
        player->moveLeft();
    }

    if(moveRight){
        player->moveRight();
    }
    if(moveUp){
        player->moveUp();
    }

    if(moveDown){
        player->moveDown();
    }


    if(timeCounter%1 || timeCounter==1){
      makeSound(0);}


  }
  else{
      if(!items.isEmpty()){
              for(auto it=items.begin(); it!=items.end(); it++){

                  if(it.value()->getType() == ItemOfScene::typeOfItem::BUBBLE ){
                      auto bubble = (qgraphicsitem_cast<Bubble*>(it.value()));
                      bubble->move(40);
                  }
              }
      }

      end_animation();
  }
  timeCounter++;
}



void LevelMermaid::clean_mem()
{


    for(auto cc=items.begin(); cc != items.end(); cc++){


                if(typeid (*cc.value())== typeid(EnemyofMermaid) ){
                    if(qgraphicsitem_cast<EnemyofMermaid*>(cc.value())->getIsAlive()){
                        delete(qgraphicsitem_cast<EnemyofMermaid*>(cc.value())->healthBar->bar);
                        delete(qgraphicsitem_cast<EnemyofMermaid*>(cc.value())->healthBar->barFrame);
                    }
                }

                if(typeid (*cc.value())!= typeid(EnemyOctopus))
                    delete cc.value();

            }


    items.clear();
    deleteSounds();
    if(onRules)
        removeItem(rules);

}

void LevelMermaid::end_animation()
{
    stopEventKey=true;

    enemyOctopus->setPixmap(QPixmap(":/images/enemies/octopus/octopusGreen.png"));
    Bubble *bubble = new Bubble();
    items.insert(bubble->getID(),bubble);
    //nt random= rand(300,780);

    int x = QRandomGenerator::global()->bounded(600, 1300);
    bubble->setPos(x, 800);
    addItem(bubble);
    bubble->incID();
    enemyOctopus->end_image_change();



    if(!positionPlayer){
        if(player->pos().y()<height()/2){
             player->setPos(player->x(), player->y()+10);

        }
        else if(player->pos().y()>height()/2){
            player->setPos(player->x(), player->y()-10);
        }
        else
        positionPlayer=true;
    }
    else{
        if(player->x()<950)
        player->setPos(player->x()+10, player->y());
        else{
            clean_mem();
            delete(enemyOctopus);
            emit levelCompleted();
            return;
        }
    }


}










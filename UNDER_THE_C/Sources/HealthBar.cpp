#include "Headers/HealthBar.h"

#include <QPainter>





HealthBar::HealthBar(float x, float y, float width, float height)
{
    barFrame = new QGraphicsRectItem;
    barFrame->setRect(x, y, width, height);
    barFrame->setBrush(Qt::gray);


    bar = new QGraphicsRectItem(x,y, width, height);
    bar->setBrush(Qt::cyan);
}

HealthBar::~HealthBar()
{
    delete this->bar;
    delete this->barFrame;

}



#include <Headers/DoorPinky.h>

#include <QGraphicsScene>
#include "QPainter"
#include <stdlib.h>
#include <QTimer>
#include <QDebug>
#include <Headers/PlayerPinky.h>
#include <Headers/LevelPinky.h>
int DoorPinky::brojac=0;
DoorPinky::DoorPinky(QGraphicsItem *parent)
{
        setType(DOORP);
        setIsAlive(true);
        setPixmap(QPixmap(":/images/other/pinky_scene/door_star.png"));
        setScale(1.1);
        ID=brojac;
        brojac++;
}

/*void DoorPinky::collision(DoorPinky* it){
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i = 0; i < n; i++){
        if(typeid (*(colliding_items[i])) == typeid (PlayerPinky)){
            auto player = (qgraphicsitem_cast<PlayerPinky*>(colliding_items[i]));

        }
    }
}*/

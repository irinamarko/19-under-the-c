#include "Headers/PlayerMermaid.h"

#include <iostream>

PlayerMermaid::PlayerMermaid(QGraphicsItem *parent): QGraphicsPixmapItem(parent){

    setPixmap(QPixmap(":/images/players/mermaid/mermaid_1.png"));
    setScale(0.1);
    number_of_bubbles=0;
    shiled=false;


    img_change = 0;

}
void PlayerMermaid::image_change()
{
    if(img_change <= 3){
        if(shiled==false){
         setPixmap(QPixmap(":/images/players/mermaid/mermaid_1.png"));
         setScale(0.1);
        }
        else{
         setPixmap(QPixmap(":/images/players/mermaid/shiled_1.png"));
         setScale(0.22);
        }

        img_change++;
    }
    else if(img_change >3 && img_change <= 6){
        if(shiled==false){
         setPixmap(QPixmap(":/images/players/mermaid/mermaid_2.png"));
         setScale(0.1);
        }
        else{
         setPixmap(QPixmap(":/images/players/mermaid/shiled_2.png"));
         setScale(0.22);
        }

        img_change++;
    }
    else if(img_change > 6 && img_change <=9){
        if(shiled==false){
         setPixmap(QPixmap(":/images/players/mermaid/mermaid_3.png"));
         setScale(0.1);
        }
        else{
         setPixmap(QPixmap(":/images/players/mermaid/shiled_3.png"));
         setScale(0.22);
        }
        img_change++;
    }
    else if(img_change >9){
        if(shiled==false){
         setPixmap(QPixmap(":/images/players/mermaid/mermaid_2.png"));
         setScale(0.1);
        }
        else{
         setPixmap(QPixmap(":/images/players/mermaid/shiled_2.png"));
         setScale(0.22);
        }
        img_change++;
        if(img_change == 13)
            img_change = 0;
    }

}

int PlayerMermaid::get_number_of_bubbles()
{
   return number_of_bubbles++;
}
void PlayerMermaid::in_number_of_bubbles(){
    number_of_bubbles++;
}



bool PlayerMermaid::getShiled()
{
    return shiled;
}

void PlayerMermaid::setShiled(bool state)
{
    shiled=state;
}

void PlayerMermaid::moveLeft()
{
    if(pos().x()>0)
    setPos(x()-10,y());
}
void PlayerMermaid::moveRight()
{
    if(pos().x()<1130)
    setPos(x()+10,y());
}
void PlayerMermaid::moveUp()
{
    if(pos().y()>0)
    setPos(x(),y()-10);
}
void PlayerMermaid::moveDown()
{
    if(pos().y()<625)
    setPos(x(),y()+10);
}

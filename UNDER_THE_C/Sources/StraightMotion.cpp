#include "Headers/StraightMotion.h"

StraightMotion::StraightMotion(QGraphicsItem *parent): ItemOfScene(parent)
{

}

void StraightMotion::move(int speed_item)
{
    setPos(x(),y()-10-speed_item);

    // Ako je van scene brise ga
    if (y() + height() < 0){
        setIsAlive(false);
    }
}

void StraightMotion::move(int side, int speed, int scene_border)
{
    // Pravolinijsko kretanje na dole
    if(side == 0){
        setPos(x(), y() + speed);

        if(y() > scene_border)
            setIsAlive(false);
    }
    // Pravolinijsko kretanje na levo
    else if(side == 1){
        setPos(x() - speed, y());

        if(x() + width() < 0)
            setIsAlive(false);
    }
    // Pravolinijsko kretanje na desno
    else if(side == 2){
        setPos(x() + speed, y());

        if(x() > scene_border)
            setIsAlive(false);
    }
}

int StraightMotion::height()
{
    return boundingRect().height()*0.1;
}

int StraightMotion::width()
{
    return boundingRect().width();
}

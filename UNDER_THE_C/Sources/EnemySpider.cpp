#include "Headers/EnemySpider.h"

EnemySpider::EnemySpider()
{
    setType(ENEMYSPIDER);
    setIsAlive(true);

    health = 5;
    health_change = 0;

    setPos(16*80,70*16);
    setPixmap(QPixmap(":/images/enemies/spider/spiderboss1.png"));
    setScale(0.5);
    setZValue(1);

    timeout = 0;
    attack_mode = false;
    attack_cooldown = 0;
    img_change = 0;
}

void EnemySpider::findPlayer()
{
    const auto zone = scene()->items(mapToScene(QRect(-41*80*2,-9*70*2,74*80*2,23*70*2)));

    for(auto item: zone){

        if(item == this)
            continue;

        if(typeid (*(item)) == typeid(PlayerPlatform)){

            if(attack_cooldown == 0){
                attack_mode = true;
                attack_cooldown++;
            }
            else{
                attack_cooldown++;

                if(attack_cooldown == 50)
                    attack_cooldown = 0;
            }
        }
    }
}

void EnemySpider::collision()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (PlayerPlatform)){

            auto player = (qgraphicsitem_cast<PlayerPlatform *>(colliding_items[i]));

            if(player->invincible == 0 && getIsAlive()){
                health_change = 1;
                player->invincible++;
            }
        }
        else if(typeid (*(colliding_items[i])) == typeid (Sword) && timeout == 0){
            timeout = 10;
            health--;

            if(health <= 0)
                setIsAlive(false);
        }
    }
}

void EnemySpider::image_change()
{

    if(img_change <= 4){
        setPixmap(QPixmap(":/images/enemies/spider/spiderboss1.png"));
        img_change++;
    }
    else{
        setPixmap(QPixmap(":/images/enemies/spider/spiderboss2.png"));
        img_change++;
        if(img_change >= 8)
            img_change = 0;
    }
}

void EnemySpider::decreaseTimeout()
{
    timeout--;
}

int EnemySpider::getTimeout()
{
    return timeout;
}

bool EnemySpider::getAttackMode()
{
    return attack_mode;
}

void EnemySpider::setAttackMode(bool value)
{
    attack_mode = value;
}

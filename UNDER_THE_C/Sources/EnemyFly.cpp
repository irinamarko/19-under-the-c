#include "Headers/EnemyFly.h"

EnemyFly::EnemyFly()
{
    setIsAlive(true);
    setType(ENEMYGHOST);

    setPixmap(QPixmap(":/images/enemies/ghost/ghost_inactive_1.png"));
    setScale(0.75);

    health = 3;
    timeout = 0;
    nearPlayer = false;
    dx = 0;
    dy = 0;
    image_parameter = 0;
    health_change = 0;
    setMusicEmit(0);

}
// Kolizija neprijatelja i ostalih objekata na sceni
void EnemyFly::collisions()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (Sword)){
            setMusicEmit(1);
            timeout = 10;
            health--;

            // Udareni neprijatelj se pomera dalje od igraca
            setPos(x() + dx*(-5), y() + dy*(-4));


            // Provera da li neprijatelj ima jos zivota
            if(health <= 0)
                setIsAlive(false);
        }
        if(typeid (*(colliding_items[i])) == typeid (Stars)){

            timeout = 10;
            health--;

            setPos(x() + dx*(-5), y() + dy*(-4));

            if(health <= 0)
                setIsAlive(false);
        }
        if(typeid (*(colliding_items[i])) == typeid (StarsL)){

            timeout = 10;
            health--;

            setPos(x() + dx*(-5), y() + dy*(-4));

            if(health <= 0)
                setIsAlive(false);
        }
        if(typeid (*(colliding_items[i])) == typeid (PlayerPinky)){
            auto player = (qgraphicsitem_cast<PlayerPinky *>(colliding_items[i]));

            if(player->invincible == 0 && getIsAlive()){
                setHealthChange(1);
                player->invincible++;
            }
        }
        if(typeid (*(colliding_items[i])) == typeid (PlayerPlatform)){
            auto player = (qgraphicsitem_cast<PlayerPlatform *>(colliding_items[i]));

            if(player->invincible == 0 && getIsAlive()){
                setHealthChange(1);
                player->invincible++;
            }
        }
        if(typeid (*(colliding_items[i])) == typeid (EnemyFly)){

            int random = rand()%3;
            if(random == 0)
                setPos(x() + dx*(-2), y() + dy*(-2));
            else if(random == 1)
                setPos(x() + dx*(-2), y());
            else
                setPos(x(), y() + dy*(-2));

            timeout = 3;
        }

        if(typeid (*(colliding_items[i])) == typeid (Platform)){

            auto platform = (qgraphicsitem_cast<Platform *>(colliding_items[i]));
            collide_platform(platform->pos().x(), platform->pos().y(), platform->rect().width(), platform->rect().height());
        }
    }
}
/*
void EnemyFly::collisionsPinky()
{

    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (Stars)){

            timeout = 10;
            health--;

            // Udareni neprijatelj se pomera dalje od igraca
            setPos(x() + dx*(-5), y() + dy*(-4));


            // Provera da li neprijatelj ima jos zivota
            if(health <= 0)
                setIsAlive(false);
        }
        if(typeid (*(colliding_items[i])) == typeid (StarsL)){

            timeout = 10;
            health--;

            // Udareni neprijatelj se pomera dalje od igraca
            setPos(x() + dx*(-5), y() + dy*(-4));


            // Provera da li neprijatelj ima jos zivota
            if(health <= 0)
                setIsAlive(false);

        }
        if(typeid (*(colliding_items[i])) == typeid (PlayerPinky)){
            if(getIsAlive()){
                setHealthChange(1);
            }
        }
        if(typeid (*(colliding_items[i])) == typeid (EnemyFly)){

            /*auto object = (qgraphicsitem_cast<EnemyFly *>(colliding_items[i]));
            int ox = object->x();
            int oy = object->y();



            int random = rand()%3;
            if(random == 0)
                setPos(x() + dx*(-2), y() + dy*(-2));//-2
            else if(random == 1)
                setPos(x() + dx*(-2), y());
            else
                setPos(x(), y() + dy*(-2));

            //std::cout << random << std::endl;
            timeout = 3;
        }

        if(typeid (*(colliding_items[i])) == typeid (Platform)){

            auto platform = (qgraphicsitem_cast<Platform *>(colliding_items[i]));
            collide_platform(platform->pos().x(), platform->pos().y(), platform->rect().width(), platform->rect().height());
        }
    }
}*/


// Kretanje neprijatelja
void EnemyFly::move(int player_x, int player_y)
{
    vectorMove(player_x, player_y);
    setPos(x() + dx, y() + dy);
}
// Neprijatelj proverava da li je igrac u blizini
void EnemyFly::findPlayer()
{
    //WARNING: Sve su dimenzije 0.75 puta manje
    const auto zone = scene()->items(mapToScene(QRect(-200/0.75, -200/0.75, 450/0.75, 450/0.75)));

    for (auto item: zone){

        if (item == this)
            continue;

        if(typeid (*(item)) == typeid (PlayerPlatform) || typeid (*(item)) == typeid (PlayerPinky))
            nearPlayer = true;
    }

}
/*
void EnemyFly::findPlayerPinky()
{
    //WARNING: Sve su dimenzije 0.2 puta manje
    const auto zone = scene()->items(mapToScene(QRect(-50*5, -50*5, 200*5, 200*5)));

    for (auto item: zone){

        if (item == this)
            continue;

        if(typeid (*(item)) == typeid (PlayerPinky))
            nearPlayer = true;
    }

}*/

void EnemyFly::vectorMove(int player_x, int player_y)
{

    // player_x+30 je x koordinata sredista igraca,
    // player_y+70 je y koordinata sredista igraca
    if(x() < player_x+30)
        dx = 10;
    else if(x() > player_x+30)
        dx = -10;

    if(y() < player_y+70)
        dy = 5;
    else if(y() > player_y+70)
        dy = -5;

}

void EnemyFly::image_change()
{
    // Ako je neprijatelj pogodjen iscrtavamao jednu sliku
    if(timeout > 5){
        if(dx > 0)
            setPixmap(QPixmap(":/images/enemies/ghost/ghost_hit.png"));
        else
            setPixmap(QPixmap(":/images/enemies/ghost/ghost_hit_left.png"));
    }
    // Ako neprijatelj miruje smenjujemo tri slike mirovanja
    else if(nearPlayer == false){

        if(image_parameter >= 0 && image_parameter < 7){
            setPixmap(QPixmap(":/images/enemies/ghost/ghost_inactive_1.png"));
            image_parameter++;
        }
        else if(image_parameter >= 7 && image_parameter < 14){
            setPixmap(QPixmap(":/images/enemies/ghost/ghost_inactive_2.png"));
            image_parameter++;
        }
        else if(image_parameter >= 14 && image_parameter < 21){
            setPixmap(QPixmap(":/images/enemies/ghost/ghost_inactive_3.png"));
            image_parameter++;
        }
        else
            image_parameter = 0;
    }
    else{
        // Neprijatelj se krece nadesno
        if(dx > 0){
            if(image_parameter >= 0 && image_parameter < 7){
                setPixmap(QPixmap(":/images/enemies/ghost/ghost_active_1.png"));
                image_parameter++;
            }
            else if(image_parameter >= 7 && image_parameter < 14){
                setPixmap(QPixmap(":/images/enemies/ghost/ghost_active_2.png"));
                image_parameter++;
            }
            else
                image_parameter = 0;
        }
        // Neprijatelj se krece nalevo
        else{
            if(image_parameter >= 0 && image_parameter < 7){
                setPixmap(QPixmap(":/images/enemies/ghost/ghost_active_left_1.png"));
                image_parameter++;
            }
            else if(image_parameter >= 7 && image_parameter < 14){
                setPixmap(QPixmap(":/images/enemies/ghost/ghost_active_left_2.png"));
                image_parameter++;
            }
            else
                image_parameter = 0;

        }
    }
}
// Pomocna funkcija za racunanje i popravljanje kolizija neprijatelja sa platformama
void EnemyFly::collide_platform(int platform_x, int platform_y, int platform_width, int platform_height)
{
    //kolizija neprijatelja i gornje ivice platforme
    if(pos().y() + boundingRect().height()*0.75 - 5 <= platform_y){//*0.2
        setPos(x(), y() - 5);
        return;
    }
    //kolizija neprijatelja i donje ivice platforme
    if(pos().y() + 5 >= platform_y + platform_height){//+5
        setPos(x(), y() + 5);
        return;
    }
    // kolizija neprijatelja i leve ivice platforme
    if(pos().x() +  /*boundingRect().width()*0.75*/75 - 11 <= platform_x){//50//*0.2
        setPos(platform_x-boundingRect().width()*0.75, y());
        //setPos(x() - 10, y());//-10
        return;
    }
    //kolizija neprijatelja i desne ivice platforme
    if(pos().x() + 11 >= platform_x + platform_width){
        setPos(x() + 10, y());
         return;
    }
}

bool EnemyFly::getNearPlayer()
{
    return nearPlayer;
}

void EnemyFly::setNearPlayer(bool p)
{
    nearPlayer = p;
}

int EnemyFly::getTimeout()
{
    return timeout;
}

void EnemyFly::decraseTimeout()
{
    timeout--;
}

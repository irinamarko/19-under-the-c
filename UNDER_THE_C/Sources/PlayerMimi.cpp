//#include "Key.h"
#include "Headers/PlayerMimi.h"
#include <QGraphicsScene>
//#include "Enemy.h"
#include <QKeyEvent>
#include <iostream>
PlayerMimi::PlayerMimi(QGraphicsItem *parent): QGraphicsPixmapItem(parent){

 setPixmap(QPixmap(":/images/players/mimi/mimi_1.png"));

 setScale(0.1);
 number_of_fires=0;

    img_change = 0;
}
void PlayerMimi::image_change()
{
    if(img_change <= 3){
        setPixmap(QPixmap(":/images/players/mimi/mimi_1.png"));
        img_change++;
    }
    else if(img_change >3 && img_change <= 6){
        setPixmap(QPixmap(":/images/players/mimi/mimi_2.png"));
        img_change++;
    }
    else if(img_change > 6 && img_change <= 9){
        setPixmap(QPixmap(":/images/players/mimi/mimi_3.png"));
        img_change++;
    }
    else if(img_change > 9){
        setPixmap(QPixmap(":/images/players/mimi/mimi_4.png"));
        img_change++;
        if(img_change == 13)
            img_change = 0;
    }
    // std::cout<<img_change<<std::endl;
}



int PlayerMimi::get_number_of_fires(){
    return number_of_fires;
}

void PlayerMimi:: in_number_of_fires(){
    number_of_fires++;
}

bool PlayerMimi::getIsAlive()
{
    return isAlive;
}

void PlayerMimi::setIsAlive(bool alive){
    isAlive=alive;
}

void PlayerMimi::moveLeft()
{
    if(pos().x()>0)
        setPos(x()-10, y());
}

void PlayerMimi::moveRight()
{
    if(pos().x()<700)
        setPos(x()+10, y());
}





#ifndef MENUSCENE_H
#define MENUSCENE_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <Headers/Button.h>
#include <QGraphicsItem>
class MenuScene: public QGraphicsScene
{
    Q_OBJECT
public:
    MenuScene(QObject * parent = nullptr);

    void initialize();
signals:
    //ovo mi je signal koji cu koristiti da pozovem izvrsavanje igrice
    void play();
};

#endif // MENUSCENE_H

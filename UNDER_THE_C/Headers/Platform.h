#ifndef PLATFORM_H
#define PLATFORM_H

#include <QGraphicsRectItem>
#include <QPainter>
#include <Headers/StaticSceneObjects.h>

class Platform: public StaticSceneObjects, public QGraphicsRectItem//, public QPaintDevice
{
    Q_OBJECT
public:
    Platform();
    Platform(char type);
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget=0);
protected:
    QPixmap image_type;
    int x,y,w,h;

};

#endif // PLATFORM_H

#ifndef TIMER_H
#define TIMER_H

#include <QGraphicsTextItem>
#include<QGraphicsPixmapItem>
class Timer: public QGraphicsTextItem{
public:
    Timer(QGraphicsItem * parent=0);
    void nextSecond();

private:
    int number_of_second;
};

#endif // TIMER_H

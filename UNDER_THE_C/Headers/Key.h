#ifndef KEY_H
#define KEY_H
#include <QGraphicsRectItem>
#include <QObject>
#include "ItemOfScene.h"
#include <Headers/StraightMotion.h>
#include <Headers/Collectable.h>

class Key: public StraightMotion, public Collectable{
    Q_OBJECT
public:
    Key(QGraphicsItem * parent=0);
    void collision();
    static double ubrzanje_key;
    static int brojac;
    int ID;
};
#endif // KEY_H



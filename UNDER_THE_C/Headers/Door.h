#ifndef DOOR_H
#define DOOR_H

#include <Headers/Platform.h>
#include <Headers/ItemOfScene.h>
#include <Headers/PlayerPlatform.h>
#include <Headers/ItemWithSounds.h>

class Door:public ItemOfScene, public ItemWithSounds
{
    Q_OBJECT
public:
    Door(QObject* parent = nullptr);
    Door(char type);
    void collision();
private:
    char type;
};

#endif // DOOR_H

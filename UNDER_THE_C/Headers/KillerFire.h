#ifndef KILLERFIRE_H
#define KILLERFIRE_H
#include <QDebug>
#include <Headers/ItemOfScene.h>
#include <QGraphicsItem>
#include <Headers/StraightMotion.h>
#include <Headers/Enemies.h>

class KillerFire: public StraightMotion, public Enemies{
public:

    KillerFire(int x=0, int y=0, QGraphicsItem *parent=0);
    int height()const;
    void collision();

};

#endif // KILLERFIRE_H

#ifndef PLAYERPINKY_H
#define PLAYERPINKY_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <Headers/PlayerPlatform.h>

class PlayerPinky: public PlayerPlatform/*public QObject, public QGraphicsPixmapItem*/{
    Q_OBJECT
public:
    PlayerPinky(QGraphicsItem * parent=0);
    void image_change();
    //void collisions();
    int img_side;
    int num_of_keys;
    int num_of_stars;
    int invincible;
    void CollisionDoor();
private:
    //int number_of_lianas;
    int img_change=0;
};

/*class PlayerPinky:public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    PlayerPinky(QGraphicsItem * parent=0);
    //void keyPressEvent(QKeyEvent * event);
    QGraphicsPixmapItem *liana;
    int get_number_of_lianas();
    void in_number_of_lianas();

    ************ Dodato ***************
    QPainterPath shape() const override;
private:
    int number_of_lianas;

//void jump();
};
*/
#endif // PLAYERPINKY_H

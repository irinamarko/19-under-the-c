#ifndef ENEMIES_H
#define ENEMIES_H


class Enemies
{
public:
    Enemies();
    int getHealthChange();
    void setHealthChange(int value);
protected:
    int health_change;
    int health;
};

#endif // ENEMIES_H

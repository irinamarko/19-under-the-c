#ifndef COLLECTABLE_H
#define COLLECTABLE_H


class Collectable
{
public:
    Collectable();
    int getScoreChange();
    void setScoreChange(int value);
protected:
    int score_change;

};

#endif // COLLECTABLE_H

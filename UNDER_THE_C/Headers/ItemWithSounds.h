#ifndef ITEMWITHSOUNDS_H
#define ITEMWITHSOUNDS_H

class ItemWithSounds{
public:
    void setMusicEmit(int value);
    int getMusicEmit();

private:
    int musicEmit;
};


#endif // ITEMWITHSOUNDS_H

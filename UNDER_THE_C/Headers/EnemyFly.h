#ifndef ENEMYFLY_H
#define ENEMYFLY_H

#include <Headers/ItemOfScene.h>
#include <QGraphicsScene>
#include <Headers/PlayerPlatform.h>
#include <Headers/Platform.h>
#include <QPainterPath>
#include <Headers/Sword.h>
#include <Headers/Enemies.h>
#include <Headers/PlayerPinky.h>
#include <Headers/Stars.h>
#include <Headers/StarsL.h>
#include <Headers/ItemWithSounds.h>

class EnemyFly: public ItemOfScene, public Enemies, public ItemWithSounds
{
    Q_OBJECT
public:
    EnemyFly();
    void collisions();
    //void collisionsPinky();
    void move(int player_x, int player_y);
    void findPlayer();
    //void findPlayerPinky();
    bool getNearPlayer();
    void image_change();
    void setNearPlayer(bool p);
    int getTimeout();
    void decraseTimeout();
    //int id;
private:
    void collide_platform(int platform_x, int platform_y, int platform_width, int platform_height);
    void vectorMove(int player_x, int player_y);
    int image_parameter;
    bool nearPlayer;
    int timeout;
    int dx;
    int dy;
};

#endif // ENEMYFLY_H

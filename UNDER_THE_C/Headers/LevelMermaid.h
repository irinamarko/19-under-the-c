#ifndef LEVELMERMAID_H
#define LEVELMERMAID_H

#include <Headers/Levelscene.h>
#include <Headers/PlayerMermaid.h>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QMediaPlayer>
#include <Headers/EnemyofMermaid.h>
#include <Headers/Bubble.h>
#include <Headers/Shell.h>
#include <Headers/HealthBar.h>
#include <Headers/Music.h>
#include <Headers/EnemyOctopus.h>
#include <Headers/Ink.h>
class LevelMermaid: public LevelScene
{
    Q_OBJECT
public:
    LevelMermaid(QObject * parent = nullptr);
    void initialize();

public slots:
    void update();
    void onSetMute();
    void onClicked();

private:
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent * event);
    void clean_mem();
    void end_animation();


    PlayerMermaid *player;
    EnemyOctopus *enemyOctopus;

    QMap<int, ItemOfScene*>items;

    int timeCounter;
    double speed_items;
    bool moveLeft;
    bool moveRight;
    bool moveUp;
    bool moveDown;
    bool finalEnemy;
    bool addedOctopus;
    bool pause;
    bool stopEventKey;
    bool end;
    bool positionPlayer;
    Button *buttonRules;
    bool onRules;



};

#endif // LEVELMERMAID_H

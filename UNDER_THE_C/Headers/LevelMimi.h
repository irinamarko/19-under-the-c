#ifndef LEVELMIMI_H
#define LEVELMIMI_H
#include <Headers/Button.h>
#include <Headers/Levelscene.h>
#include <QDebug>
#include <Headers/PlayerMimi.h>
#include <Headers/Enemy.h>
#include <Headers/Key.h>
#include <QVector>
#include <QMessageBox>
#include "WingsMini.h"
#include "WingsScore.h"
#include <QMediaPlayer>
#include <QMap>
#include "PlayerPlatform.h"
#include "Timer.h"
#include "Cloud.h"
#include "ItemOfScene.h"
#include "Fire.h"
#include "Balloon.h"
#include "KillerEnemy.h"
#include "TextOfNotice.h"

class LevelMimi: public LevelScene
{
    Q_OBJECT
public:
    LevelMimi(QObject *parent = nullptr);

    void initialize();

public slots:
    void onSetMute();
    void update();
    void onClicked();


private:

    PlayerMimi *player;
    Fire* fire;
    QMap<int, ItemOfScene*>items;
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent *event);
    void clean_mem();
    void end_animation(unsigned int game_duraton);
    void addImage(QGraphicsPixmapItem* item ,const QString &fileNime, qreal ax=0, qreal ay=0, qreal scale=1);
    double speed_items;
    WingsScore *MimisWings;
    PlayerPlatform *koko;
    bool stop;
    bool stopEventKey;
    QGraphicsPixmapItem *cage;
    QGraphicsPixmapItem *photoStopwatch;
    Timer *time;
    bool moveItems;
    bool moveLeft;
    bool moveRight;
    int indexEnemy;
    QGraphicsPixmapItem *liana;
    bool pause;
    TextOfNotice *notice;
    Button *buttonRules;
    bool onRules;
    Button *musicButton;
};

#endif // LEVELMIMI_H

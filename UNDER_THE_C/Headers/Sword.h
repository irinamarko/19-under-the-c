#ifndef SWORD_H
#define SWORD_H

#include <QGraphicsPixmapItem>
#include <QObject>

class Sword: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Sword();

    void move(int player_x, int player_y, int left, int right, int side);

    int getExistance();
    void setExistance(int number);
    bool getIsAlive();
    void setIsAlive(bool alive);
private:
    int existance;
    bool isAlive;
    void image_change(int left, int right, int side);
};

#endif // SWORD_H

#ifndef KILLERENEMY_H
#define KILLERENEMY_H
#include <Headers/Enemy.h>
#include <Headers/ItemOfScene.h>
#include <Headers/EnemiesWithScore.h>

class KillerEnemy:public ItemOfScene, public EnemiesWithScore
{
public:
    KillerEnemy(int numberOfLife=1,QGraphicsItem * parent=0);
    void setEndOfGame(bool value);
    void collision();
    // ItemOfScene interface
public:
    void move();

private:
    bool direction;
    int sizeOfPath;
    bool endMove;


};

#endif // KILLERENEMY_H

#ifndef SHELL_H
#define SHELL_H

#include <QObject>
#include <QPainter>
#include <Headers/StraightMotion.h>
#include <Headers/Collectable.h>

class Shell: public StraightMotion, public Collectable{
    Q_OBJECT
public:

    Shell(QGraphicsPixmapItem *parent=nullptr);
    static int counter;
    int ID;
    //void move(int speed_items);
    void collision();
    bool getOpened();
private:
    bool opened;

};

#endif // SHELL_H

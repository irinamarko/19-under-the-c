#ifndef ENEMYTHORN_H
#define ENEMYTHORN_H

#include <Headers/StaticSceneObjects.h>
#include <QGraphicsRectItem>
#include <QPainter>

class EnemyThorn:public StaticSceneObjects, public QGraphicsRectItem
{
    Q_OBJECT
public:
    EnemyThorn();
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget=0);
};

#endif // ENEMYTHORN_H

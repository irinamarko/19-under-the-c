#ifndef ENEMIESWITHSCORE_H
#define ENEMIESWITHSCORE_H

#include <Headers/Enemies.h>

class EnemiesWithScore: public Enemies
{
public:
    EnemiesWithScore();
    int getScoreChange();
    void setScoreChange(int value);
protected:
    int score_change;
};

#endif // ENEMIESWITHSCORE_H

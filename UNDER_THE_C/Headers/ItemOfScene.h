#ifndef ITEMOFSCENE_H
#define ITEMOFSCENE_H
#include <QObject>
#include <QPainter>
#include<QGraphicsPixmapItem>

class ItemOfScene:public QObject, public QGraphicsPixmapItem{
   public:
    ItemOfScene(QGraphicsItem * parent=0);
    bool getIsAlive();
    void setIsAlive(bool alive);

    static void incID()
    {
        id++;
    }

    static int getID()
    {
        return id;
    }
    enum typeOfItem{
        ENEMY,
        CLOUD,
        KILLERENEMY,
        KILLERFIRE,
        WINGS,
        KEY,
        FIRE,
        ENEMYOFMERMAID,
        ENEMYOCTOPUS,
        SHELL,
        BUBBLE,
        INK,
        ENEMYGHOST,
        DOOR,
        ENEMYSPIDER,
        DEATHRAY,
        CRISTAL,
        SPIDERBOSS,
        BALLOON,
        STAR,
        KEYP,
        DOORP,
        DOORF,
        ENEMYL,
        STARS,
        STARSL,
        TREASURE,
        FIREWORKS
    };
    void setType(typeOfItem item);
    typeOfItem getType();
    int getDX();
    void setDX(int i);
    int getDX1();
    void setDX1(int i);
protected:
    bool isAlive;
private:
    int dx, dx1;
    static int id;
    typeOfItem type;


};

#endif // ITEMOFSCENE_H

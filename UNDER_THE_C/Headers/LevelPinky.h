#ifndef LEVELPINKY_H
#define LEVELPINKY_H

#include <Headers/Levelscene.h>
#include <QKeyEvent>
#include <QDebug>
#include <Headers/PlayerPinky.h>
#include <Headers/Enemy.h>
#include <Headers/KeyPinky.h>
#include <Headers/Star.h>
#include <Headers/Stars.h>
#include <Headers/DoorPinky.h>
#include <Headers/Treasure.h>
//#include <Liana.h>

#include <Headers/Platform.h>

#include <QVector>
#include <QMap>
class LevelPinky: public LevelScene
{
    Q_OBJECT
public:
    LevelPinky(QObject * parent = nullptr);
    void initialize();
    void DeleteLevelPinky();
    void Animation();
   // void addObjects(ItemOfScene::typeOfItem type, int x, int y);
public slots:
    void onSetMute();
    void update();
    void onClicked();
private:
    PlayerPinky *player;
    QMap<int, ItemOfScene*> items;
    int dx, dx1;
    bool pause;
    bool stopEventKey;
    int music;
    QGraphicsPixmapItem *fireworks;
    QGraphicsPixmapItem *mimi;
    QGraphicsPixmapItem *mermaid;
    QGraphicsPixmapItem *platf;

    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void playerCollide();

    int collision_side[4] = {0,0,0,0};
    int left_right_jump[3] = {0,0,0};

    void addImage(QGraphicsPixmapItem* item ,const QString &fileNime, qreal ax=0, qreal ay=0, qreal scale=1);

    void addPlatform(int x, int y, int width, int height);
    QVector<Platform*> platforme;
    bool endAnimation;
    int wait;
    int oldWait;
    Button *buttonRules;
    bool onRules;
    Button *musicButton;
};

#endif // LEVELPINKY_H
